<?php

namespace App\Frbs\HmBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Frbs\HmBundle\Repository\DeviceRepository")
 */
class Device
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, options={"default":"Описание компьютера"})
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $os_version;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $service_version;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $logged_on_user;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $uptime;

    /**
     * @ORM\Column(type="integer")
     */
    private $ram_total;

    /**
     * @ORM\Column(type="datetime", options={"default":"CURRENT_TIMESTAMP"})
     */
    private $last_online;

    /**
     * @ORM\Column(type="datetime", options={"default":"CURRENT_TIMESTAMP"})
     */
    private $created_at;

    /**
     * @var HardwareCpu[]|ArrayCollection
     *
     * @ORM\OneToMany(
     *      targetEntity="HardwareCpu",
     *      mappedBy="device",
     *      orphanRemoval=true,
     *      cascade={"persist", "remove"}
     * )
     * @Serializer\Exclude()
     */
    private $cpu;

    /**
     * @var HardwareGpu[]|ArrayCollection
     *
     * @ORM\OneToMany(
     *      targetEntity="HardwareGpu",
     *      mappedBy="device",
     *      orphanRemoval=true,
     *      cascade={"persist", "remove"}
     * )
     * @Serializer\Exclude()
     */
    private $gpu;

    /**
     * @var HardwareMb[]|ArrayCollection
     *
     * @ORM\OneToMany(
     *      targetEntity="HardwareMb",
     *      mappedBy="device",
     *      orphanRemoval=true,
     *      cascade={"persist", "remove"}
     * )
     * @Serializer\Exclude()
     */
    private $mb;

    /**
     * @var HardwareRam[]|ArrayCollection
     *
     * @ORM\OneToMany(
     *      targetEntity="HardwareRam",
     *      mappedBy="device",
     *      orphanRemoval=true,
     *      cascade={"persist", "remove"}
     * )
     * @Serializer\Exclude()
     */
    private $ram;

    /**
     * @var HardwareHdd[]|ArrayCollection
     *
     * @ORM\OneToMany(
     *      targetEntity="HardwareHdd",
     *      mappedBy="device",
     *      orphanRemoval=true,
     *      cascade={"persist", "remove"}
     * )
     * @Serializer\Exclude()
     */
    private $hdd;

    /**
     * @var LocalUser[]|ArrayCollection
     *
     * @ORM\OneToMany(
     *      targetEntity="LocalUser",
     *      mappedBy="device",
     *      orphanRemoval=true,
     *      cascade={"persist", "remove"}
     * )
     * @Serializer\Exclude()
     */
    private $local_user;

    /**
     * @var HardwareVolume[]|ArrayCollection
     *
     * @ORM\OneToMany(
     *      targetEntity="HardwareVolume",
     *      mappedBy="device",
     *      orphanRemoval=true,
     *      cascade={"persist", "remove"}
     * )
     * @Serializer\Exclude()
     */
    private $volume;

    /**
     * @var HardwarePrinter[]|ArrayCollection
     *
     * @ORM\OneToMany(
     *      targetEntity="HardwarePrinter",
     *      mappedBy="device",
     *      orphanRemoval=true,
     *      cascade={"persist", "remove"}
     * )
     * @Serializer\Exclude()
     */
    private $printer;

    /**
     * @var HardwareDisplay[]|ArrayCollection
     *
     * @ORM\OneToMany(
     *      targetEntity="HardwareDisplay",
     *      mappedBy="device",
     *      orphanRemoval=true,
     *      cascade={"persist", "remove"}
     * )
     * @Serializer\Exclude()
     */
    private $display;

    /**
     * @var HardwareAdapter[]|ArrayCollection
     *
     * @ORM\OneToMany(
     *      targetEntity="HardwareAdapter",
     *      mappedBy="device",
     *      orphanRemoval=true,
     *      cascade={"persist", "remove"}
     * )
     */
    private $adapter;

    /**
     * @var Alert[]|ArrayCollection
     *
     * @ORM\OneToMany(
     *      targetEntity="Alert",
     *      mappedBy="device",
     *      orphanRemoval=true,
     *      cascade={"persist", "remove"}
     * )
     * @ORM\OrderBy({"created_at" = "DESC"})
     */
    private $alert;

    /**
     * @var SoftwareList[]|ArrayCollection
     *
     * @ORM\OneToMany(
     *      targetEntity="SoftwareList",
     *      mappedBy="device",
     *      orphanRemoval=true,
     *      cascade={"persist", "remove"}
     * )
     * @ORM\OrderBy({"name" = "ASC"})
     * @Serializer\Exclude()
     */
    private $software;

    /**
     * @var Group
     *
     * @ORM\ManyToOne(targetEntity="DeviceGroup", inversedBy="device")
     * @ORM\JoinColumn(nullable=false)
     */
    private $group;

    public function __construct()
    {

    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getHdd(): Collection
    {
        return $this->hdd;
    }

    public function getIpAddress(): ?string
    {
        return $this->ip_address;
    }

    public function setIpAddress(string $ip_address): self
    {
        $this->ip_address = $ip_address;

        return $this;
    }

    public function getGroup(): ?DeviceGroup
    {
        return $this->group;
    }

    public function setGroup(?DeviceGroup $group): void
    {
        $this->group = $group;
    }

    public function getAlertList(): ?Collection
    {
        return $this->alert;
    }

    public function setAlertList(?Alert $alert): void
    {
        $this->alert = $alert;
    }

    public function getSoftwareList(): ?Collection
    {
        return $this->software;
    }

    public function setSoftwareList(?SoftwareList $software): void
    {
        $this->software = $software;
    }

    public function getDisplayList(): ?Collection
    {
        return $this->display;
    }

    public function setDisplayList(?HardwareDisplay $display): void
    {
        $this->display = $display;
    }

    public function getPrinterList(): ?Collection
    {
        return $this->printer;
    }

    public function setPrinterList(?HardwarePrinter $printer): void
    {
        $this->printer = $printer;
    }

    public function getVolumeList(): ?Collection
    {
        return $this->volume;
    }

    public function setVolumeList(?HardwareVolume $volume): void
    {
        $this->volume = $volume;
    }

    public function getLocalUserList(): ?Collection
    {
        return $this->local_user;
    }

    public function setLocalUserList(?LocalUser $local_user): void
    {
        $this->local_user = $local_user;
    }

    public function getAdapterList(): ?Collection
    {
        return $this->adapter;
    }

    public function setAdapterList(?HardwareAdapter $adapter): void
    {
        $this->adapter = $adapter;
    }

    public function getCpuList(): ?Collection
    {
        return $this->cpu;
    }

    public function setCpuList(?HardwareCpu $cpu): void
    {
        $this->cpu = $cpu;
    }

    public function getGpuList(): ?Collection
    {
        return $this->gpu;
    }

    public function setGpuList(?HardwareGpu $gpu): void
    {
        $this->gpu = $gpu;
    }

    public function getHddList(): ?Collection
    {
        return $this->hdd;
    }

    public function setHddList(?HardwareHdd $hdd): void
    {
        $this->hdd = $hdd;
    }

    public function getMbList(): ?Collection
    {
        return $this->mb;
    }

    public function setMbList(?HardwareMb $mb): void
    {
        $this->mb = $mb;
    }

    public function getRamList(): ?Collection
    {
        return $this->ram;
    }

    public function setRamList(?HardwareRam $ram): void
    {
        $this->ram = $ram;
    }

}
