<?php

namespace App\Frbs\HmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Frbs\HmBundle\Repository\SoftwareListRepository")
 * @ORM\Table(name="software_list")
 */
class SoftwareList
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $publisher;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $version;

    /**
     * @var Device
     *
     * @Serializer\Exclude()
     * @ORM\ManyToOne(targetEntity="Device", inversedBy="software")
     * @ORM\JoinColumn(nullable=false)
     */
    private $device;

    public function __construct()
    {

    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDevice(): ?Device
    {
        return $this->device;
    }
    public function setDevice(?Device $device): void
    {
        $this->device = $device;
    }

}
