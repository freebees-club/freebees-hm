<?php

namespace App\Frbs\HmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Frbs\HmBundle\Repository\AdapterIpRepository")
 * @ORM\Table(name="hardware_adapter_ip")
 */
class HardwareAdapterIp
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $address;

    /**
     * @var HardwareAdapter
     *
     * @ORM\ManyToOne(targetEntity="HardwareAdapter", inversedBy="ip")
     * @ORM\JoinColumn(nullable=false)
     */
    private $adapter;
    
    
    public function __construct()
    {
        
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }
    
    public function getAdapter(): ?HardwareAdapter
    {
        return $this->adapter;
    }
    public function setAdapter(?HardwareAdapter $adapter): void
    {
        $this->adapter = $adapter;
    }
    
}
