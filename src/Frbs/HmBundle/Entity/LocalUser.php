<?php

namespace App\Frbs\HmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Frbs\HmBundle\Repository\LocalUserRepository")
 * @ORM\Table(name="local_user")
 */
class LocalUser
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $name;
    
    /**
     * @ORM\Column(type="string", length=128)
     */
    private $full_name;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $description;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_local;
    
    /**
     * @ORM\Column(type="string", length=64)
     */
    private $domain;
    
    /**
     * @ORM\Column(type="boolean")
     */
    private $is_disabled;
    
    /**
     * @var Device
     *
     * @ORM\ManyToOne(targetEntity="Device", inversedBy="local_user")
     * @ORM\JoinColumn(nullable=false)
     */
    private $device;
    
    
    public function __construct()
    {
        
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
    
    public function getDevice(): ?Device
    {
        return $this->device;
    }
    public function setDevice(?Device $device): void
    {
        $this->device = $device;
    }
    
}
