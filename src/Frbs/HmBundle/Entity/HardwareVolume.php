<?php

namespace App\Frbs\HmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Frbs\HmBundle\Repository\HardwareVolumeRepository")
 * @ORM\Table(name="hardware_volume")
 */
class HardwareVolume
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $name;
    
    /**
     * @ORM\Column(type="string", length=128)
     */
    private $label;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $format;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private $available;
    
    /**
     * @ORM\Column(type="integer", length=11)
     */
    private $free;
    
    /**
     * @ORM\Column(type="integer", length=11)
     */
    private $total;
    
    /**
     * @ORM\Column(type="string", length=64)
     */
    private $type;    
    
    /**
     * @var Device
     *
     * @ORM\ManyToOne(targetEntity="Device", inversedBy="volume")
     * @ORM\JoinColumn(nullable=false)
     */
    private $device;
    
    
    public function __construct()
    {
        
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
    
    public function getDevice(): ?Device
    {
        return $this->device;
    }
    public function setDevice(?Device $device): void
    {
        $this->device = $device;
    }
    
}
