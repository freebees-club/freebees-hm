<?php

namespace App\Frbs\HmBundle\Entity;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Frbs\HmBundle\Repository\UserRepository")
 */
class User implements UserInterface, \Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", unique=true, length=64)
     */
    private $username;
    
    /**
     * @ORM\Column(type="string", length=128)
     * @Serializer\Exclude()
     */
    private $password;
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $role;    
    
    /**
     * @ORM\Column(type="string", length=64)
     */
    private $first_name;
    
    /**
     * @ORM\Column(type="string", length=64)
     */
    private $second_name;
    
    /**
     * @ORM\Column(type="string", length=64)
     */
    private $third_name;
    
    /**
     * @ORM\Column(type="string", unique=true, length=128)
     */
    private $email;
    
    /**
     * @ORM\Column(type="boolean", options={"default":0})
     */
    private $is_active;
        
    public function getId(): int
    {
        return $this->id;
    }
    function getUsername(): ?string
    {
        return $this->username;
    }

    function getPassword(): ?string
    {
        return $this->password;
    }

    function getRole(): ?string
    {
        return $this->role;
    }

    function getFirstName(): ?string
    {
        return $this->first_name;
    }

    function getSecondName(): ?string {
        return $this->second_name;
    }

    function getThirdName(): ?string {
        return $this->third_name;
    }

    function getEmail(): ?string {
        return $this->email;
    }

    function getIsActive(): bool {
        return $this->is_active;
    }

    public function getSalt()
    {
        return null;
    }    
    
    public function getRoles()
    {
        return ['ROLE_USER'];
    }
        
    function setUsername(string $username): void {
        $this->username = $username;
    }

    function setPassword(string $password): void {
        $this->password = $password;
    }

    function setRole(string $role): void {
        $this->role = $role;
    }
    
    function setFirstName(string $first_name): void {
        $this->first_name = $first_name;
    }

    function setSecondName(string $second_name): void {
        $this->second_name = $second_name;
    }

    function setThirdName(string $third_name): void {
        $this->third_name = $third_name;
    }

    function setEmail(string $email): void {
        $this->email = $email;
    }

    function setIsActive($is_active): void {
        $this->is_active = $is_active;
    }
    
    public function eraseCredentials()
    {
    }
    
    public function serialize()
    {
        return serialize([
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ]);
    }

    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
        ) = unserialize($serialized, ['allowed_classes' => false]);
    }
    
}
