<?php

namespace App\Frbs\HmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Frbs\HmBundle\Repository\HardwarePrinterRepository")
 * @ORM\Table(name="hardware_printer")
 */
class HardwarePrinter
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=128)
     */
    private $name;    

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $description;
    
    /**
     * @ORM\Column(type="string", length=128)
     */
    private $detected_error_state;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $driver_name;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_local;
    
    /**
     * @ORM\Column(type="boolean")
     */
    private $is_network;
    
    /**
     * @ORM\Column(type="string", length=128)
     */
    private $port_name;
    
    /**
     * @ORM\Column(type="boolean")
     */
    private $is_shared;   
    
    /**
     * @ORM\Column(type="boolean")
     */
    private $is_default;    
    
    /**
     * @ORM\Column(type="string", length=128)
     */
    private $share_name;    
    
    /**
     * @var Device
     *
     * @ORM\ManyToOne(targetEntity="Device", inversedBy="printer")
     * @ORM\JoinColumn(nullable=false)
     */
    private $device;
    
    
    public function __construct()
    {
        
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
    
    public function getDevice(): ?Device
    {
        return $this->device;
    }
    public function setDevice(?Device $device): void
    {
        $this->device = $device;
    }
    
}
