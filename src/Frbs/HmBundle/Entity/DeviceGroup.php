<?php

namespace App\Frbs\HmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Frbs\HmBundle\Repository\DeviceGroupRepository")
 * @ORM\Table(name="device_group")
 */
class DeviceGroup
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;
    
    /**
     * @var Device[]|ArrayCollection
     *
     * @ORM\OneToMany(
     *      targetEntity="Device",
     *      mappedBy="group",
     *      orphanRemoval=true,
     *      cascade={"persist"}
     * )
     * 
     * @Serializer\Exclude()
     */
    private $device;
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;
    
    /**
     * @ORM\Column(type="string", length=255, options={"default":"icon-screen-desktop"})
     */
    private $icon; 
    
    public function __construct()
    {

    }    

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;
        return $this;
    }
    
    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(string $icon): self
    {
        $this->icon = $icon;
        return $this;
    }

    public function getDevice(): ?Device
    {
        return $this->device;
    }
    public function setDevice(?Device $device): void
    {
        $this->device = $device;
    }
    
}
