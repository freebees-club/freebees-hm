<?php

namespace App\Frbs\HmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Frbs\HmBundle\Repository\HardwareRamRepository")
 * @ORM\Table(name="hardware_ram")
 */
class HardwareRam
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $manufacturer;

    /**
     * @ORM\Column(type="integer", length=64)
     */
    private $capacity;
    
    /**
     * @ORM\Column(type="integer", length=32)
     */
    private $speed;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $memory_type;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $part_number;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $serial_number;    
    
    /**
     * @var Device
     *
     * @ORM\ManyToOne(targetEntity="Device", inversedBy="ram")
     * @ORM\JoinColumn(nullable=false)
     */
    private $device;
    
    
    public function __construct()
    {
        
    }

    public function getId()
    {
        return $this->id;
    }
    
    public function getDevice(): ?Device
    {
        return $this->device;
    }
    public function setDevice(?Device $device): void
    {
        $this->device = $device;
    }
    
}
