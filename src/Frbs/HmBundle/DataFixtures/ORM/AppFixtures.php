<?php

namespace App\Frbs\HmBundle\DataFixtures\ORM;

use App\Frbs\HmBundle\Entity\User;
use App\Frbs\HmBundle\Entity\Settings;
use App\Frbs\HmBundle\Entity\DeviceGroup;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User;

        $user->setUsername('admin');
        $encoded = $this->encoder->encodePassword($user, '123456');
        $user->setPassword($encoded);
        $user->setRole('ROLE_ADMIN');
        $user->setFirstName('Администратор');
        $user->setSecondName('Администратор');
        $user->setThirdName('Администратор');
        $user->setEmail('admin@none.com');
        $user->setIsActive(true);
        $manager->persist($user);
        $manager->flush();

        $settings = new Settings();

        $settings->setValue('{"cpuTempThreshold":75,"gpuTempThreshold":85,"hddTempThreshold":65,"volumeThreshold":5,"vBatThreshold":2.6,"freeMemThreshold":5,"cpuLoadMonitor":true}');
        $settings->setCreatedAt(new \DateTime);
        $manager->persist($settings);
        $manager->flush();

        $group = new DeviceGroup();

        $group->setName('Нераспределенные');
        $group->setUrl('/device-group/1');
        $group->setIcon('icon-screen-desktop');
        $manager->persist($group);
        $manager->flush();
    }
}
