<?php

namespace App\Frbs\HmBundle\Repository;

use App\Frbs\HmBundle\Entity\HardwarePrinter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method HardwarePrinter|null find($id, $lockMode = null, $lockVersion = null)
 * @method HardwarePrinter|null findOneBy(array $criteria, array $orderBy = null)
 * @method HardwarePrinter[]    findAll()
 * @method HardwarePrinter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HardwarePrinterRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, HardwarePrinter::class);
    }

//    /**
//     * @return HardwarePrinter[] Returns an array of HardwarePrinter objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HardwarePrinter
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
