<?php

namespace App\Frbs\HmBundle\Repository;

use App\Frbs\HmBundle\Entity\HardwareCpu;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method HardwareCpu|null find($id, $lockMode = null, $lockVersion = null)
 * @method HardwareCpu|null findOneBy(array $criteria, array $orderBy = null)
 * @method HardwareCpu[]    findAll()
 * @method HardwareCpu[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HardwareCpuRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, HardwareCpu::class);
    }

//    /**
//     * @return HardwareCpu[] Returns an array of HardwareCpu objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HardwareCpu
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
