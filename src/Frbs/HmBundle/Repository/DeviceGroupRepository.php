<?php

namespace App\Frbs\HmBundle\Repository;

use App\Frbs\HmBundle\Entity\DeviceGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DeviceGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method DeviceGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method DeviceGroup[]    findAll()
 * @method DeviceGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DeviceGroupRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DeviceGroup::class);
    }

//    /**
//     * @return DeviceGroup[] Returns an array of DeviceGroup objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DeviceGroup
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
