<?php

namespace App\Frbs\HmBundle\Repository;

use App\Frbs\HmBundle\Entity\HardwareHdd;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method HardwareHdd|null find($id, $lockMode = null, $lockVersion = null)
 * @method HardwareHdd|null findOneBy(array $criteria, array $orderBy = null)
 * @method HardwareHdd[]    findAll()
 * @method HardwareHdd[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HardwareHddRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, HardwareHdd::class);
    }

//    /**
//     * @return HardwareHdd[] Returns an array of HardwareHdd objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HardwareHdd
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
