<?php

namespace App\Frbs\HmBundle\Repository;

use App\Frbs\HmBundle\Entity\HardwareDisplay;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method HardwareDisplay|null find($id, $lockMode = null, $lockVersion = null)
 * @method HardwareDisplay|null findOneBy(array $criteria, array $orderBy = null)
 * @method HardwareDisplay[]    findAll()
 * @method HardwareDisplay[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HardwareDisplayRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, HardwareDisplay::class);
    }

//    /**
//     * @return HardwareDisplay[] Returns an array of HardwareDisplay objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HardwareDisplay
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
