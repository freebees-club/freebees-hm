<?php

namespace App\Frbs\HmBundle\Repository;

use App\Frbs\HmBundle\Entity\HardwareAdapter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method HardwareAdapter|null find($id, $lockMode = null, $lockVersion = null)
 * @method HardwareAdapter|null findOneBy(array $criteria, array $orderBy = null)
 * @method HardwareAdapter[]    findAll()
 * @method HardwareAdapter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HardwareAdapterRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, HardwareAdapter::class);
    }

//    /**
//     * @return HardwareAdapter[] Returns an array of HardwareAdapter objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HardwareAdapter
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
