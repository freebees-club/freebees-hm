<?php

namespace App\Frbs\HmBundle\Repository;

use App\Frbs\HmBundle\Entity\Device;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Device|null find($id, $lockMode = null, $lockVersion = null)
 * @method Device|null findOneBy(array $criteria, array $orderBy = null)
 * @method Device[]    findAll()
 * @method Device[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DeviceRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Device::class);
    }

    /**
     * @return Device[] Returns an array of Device objects
     */

    public function findAllByTerm($value)
    {
        $value = mb_strtolower($value);
        $result = [];

        /** Find Device */

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('m')
            ->from('App\Frbs\HmBundle\Entity\Device', 'm')
            ->where($qb->expr()->like('m.description', $qb->expr()->literal('%' . $value . '%')))
            ->orWhere($qb->expr()->like('m.logged_on_user', $qb->expr()->literal('%' . $value . '%')))
            ->orWhere($qb->expr()->like('m.os_version', $qb->expr()->literal('%' . $value . '%')))
            ->orderBy('m.id', 'ASC')
        ;
        $devices = $qb->getQuery()->getArrayResult();
        $devices ? $result['device'] = $devices : null;

        /** Find Adapter */

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('d', 'a', 'i')
            ->from('App\Frbs\HmBundle\Entity\Device', 'd')
            ->leftJoin('d.adapter', 'a')
            ->leftJoin('a.ip', 'i')
            ->where($qb->expr()->like('a.name', $qb->expr()->literal('%' . $value . '%')))
            ->orWhere($qb->expr()->like('a.mac', $qb->expr()->literal('%' . $value . '%')))
            ->orWhere($qb->expr()->like('i.address', $qb->expr()->literal('%' . $value . '%')))
            ->orderBy('d.id', 'ASC')
        ;

        $adapters = $qb->getQuery()->getArrayResult();
        $adapters ? $result['found_adapter'] = $adapters : null;

        /** Find Cpu */

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('d', 'a')
            ->from('App\Frbs\HmBundle\Entity\Device', 'd')
            ->leftJoin('d.cpu', 'a')
            ->where($qb->expr()->like('a.name', $qb->expr()->literal('%' . $value . '%')))
            ->orderBy('d.id', 'ASC')
        ;

        $cpus = $qb->getQuery()->getArrayResult();
        $cpus ? $result['found_cpu'] = $cpus : null;

        /** Find Gpu */

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('d', 'a')
            ->from('App\Frbs\HmBundle\Entity\Device', 'd')
            ->leftJoin('d.gpu', 'a')
            ->where($qb->expr()->like('a.name', $qb->expr()->literal('%' . $value . '%')))
            ->orderBy('d.id', 'ASC')
        ;

        $gpus = $qb->getQuery()->getArrayResult();
        $gpus ? $result['found_gpu'] = $gpus : null;

        /** Find Display */

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('d', 'a')
            ->from('App\Frbs\HmBundle\Entity\Device', 'd')
            ->leftJoin('d.display', 'a')
            ->where($qb->expr()->like('a.name', $qb->expr()->literal('%' . $value . '%')))
            ->orderBy('d.id', 'ASC')
        ;

        $displays = $qb->getQuery()->getArrayResult();
        $displays ? $result['found_display'] = $displays : null;

        /** Find Hdd */

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('d', 'a')
            ->from('App\Frbs\HmBundle\Entity\Device', 'd')
            ->leftJoin('d.hdd', 'a')
            ->where($qb->expr()->like('a.name', $qb->expr()->literal('%' . $value . '%')))
            ->orderBy('d.id', 'ASC')
        ;

        $hdds = $qb->getQuery()->getArrayResult();
        $hdds ? $result['found_hdd'] = $hdds : null;

        /** Find Mb */

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('d', 'a')
            ->from('App\Frbs\HmBundle\Entity\Device', 'd')
            ->leftJoin('d.mb', 'a')
            ->where($qb->expr()->like('a.name', $qb->expr()->literal('%' . $value . '%')))
            ->orderBy('d.id', 'ASC')
        ;

        $mbs = $qb->getQuery()->getArrayResult();
        $mbs ? $result['found_mb'] = $mbs : null;

        /** Find printer */

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('d', 'a')
            ->from('App\Frbs\HmBundle\Entity\Device', 'd')
            ->leftJoin('d.printer', 'a')
            ->where($qb->expr()->like('a.name', $qb->expr()->literal('%' . $value . '%')))
            ->orderBy('d.id', 'ASC')
        ;

        $printers = $qb->getQuery()->getArrayResult();
        $printers ? $result['found_printer'] = $printers : null;

        /** Find local user */

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('d', 'a')
            ->from('App\Frbs\HmBundle\Entity\Device', 'd')
            ->leftJoin('d.local_user', 'a')
            ->where($qb->expr()->like('a.name', $qb->expr()->literal('%' . $value . '%')))
            ->orWhere($qb->expr()->like('a.full_name', $qb->expr()->literal('%' . $value . '%')))
            ->orWhere($qb->expr()->like('a.description', $qb->expr()->literal('%' . $value . '%')))
            ->orderBy('d.id', 'ASC')
        ;

        $localUsers = $qb->getQuery()->getArrayResult();
        $localUsers ? $result['found_local_user'] = $localUsers : null;

        /** Find software */

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('d', 'a')
            ->from('App\Frbs\HmBundle\Entity\Device', 'd')
            ->leftJoin('d.software', 'a')
            ->where($qb->expr()->like('a.name', $qb->expr()->literal('%' . $value . '%')))
            ->orWhere($qb->expr()->like('a.publisher', $qb->expr()->literal('%' . $value . '%')))
            ->orderBy('d.id', 'ASC')
        ;

        $softwareList = $qb->getQuery()->getArrayResult();
        $softwareList ? $result['software_list'] = $softwareList : null;

        return $result;
    }

    public function findAllCpu()
    {
        /** Find Cpu */

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('d', 'a')
            ->from('App\Frbs\HmBundle\Entity\Device', 'd')
            ->leftJoin('d.cpu', 'a')
            ->orderBy('d.id', 'ASC')
        ;

        return $qb->getQuery()->getArrayResult();
    }

    public function findAllGpu()
    {
        /** Find Gpu */

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('d', 'a')
            ->from('App\Frbs\HmBundle\Entity\Device', 'd')
            ->leftJoin('d.gpu', 'a')
            ->orderBy('d.id', 'ASC')
        ;

        return $qb->getQuery()->getArrayResult();
    }

    public function findAllHdd()
    {
        /** Find Hdd */

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('d', 'a')
            ->from('App\Frbs\HmBundle\Entity\Device', 'd')
            ->leftJoin('d.hdd', 'a')
            ->orderBy('d.id', 'ASC')
        ;

        return $qb->getQuery()->getArrayResult();
    }
    
    public function findAllDisplay()
    {
        /** Find Hdd */

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('d', 'a')
            ->from('App\Frbs\HmBundle\Entity\Device', 'd')
            ->leftJoin('d.display', 'a')
            ->orderBy('d.id', 'ASC')
        ;

        return $qb->getQuery()->getArrayResult();
    }
    
    public function findAllRam()
    {
        /** Find Hdd */

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('d', 'a')
            ->from('App\Frbs\HmBundle\Entity\Device', 'd')
            ->leftJoin('d.ram', 'a')
            ->orderBy('d.id', 'ASC')
        ;

        return $qb->getQuery()->getArrayResult();
    }
    
    public function findAllMb()
    {
        /** Find Hdd */

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('d', 'a')
            ->from('App\Frbs\HmBundle\Entity\Device', 'd')
            ->leftJoin('d.mb', 'a')
            ->orderBy('d.id', 'ASC')
        ;

        return $qb->getQuery()->getArrayResult();
    }     

}
