<?php

namespace App\Frbs\HmBundle\Repository;

use App\Frbs\HmBundle\Entity\SoftwareList;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SoftwareList|null find($id, $lockMode = null, $lockVersion = null)
 * @method SoftwareList|null findOneBy(array $criteria, array $orderBy = null)
 * @method SoftwareList[]    findAll()
 * @method SoftwareList[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SoftwareListRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SoftwareList::class);
    }

//    /**
//     * @return SoftwareList[] Returns an array of SoftwareList objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SoftwareList
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
