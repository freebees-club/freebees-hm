<?php

namespace App\Frbs\HmBundle\Repository;

use App\Frbs\HmBundle\Entity\HardwareAdapterIp;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method HardwareAdapterIp|null find($id, $lockMode = null, $lockVersion = null)
 * @method HardwareAdapterIp|null findOneBy(array $criteria, array $orderBy = null)
 * @method HardwareAdapterIp[]    findAll()
 * @method HardwareAdapterIp[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HardwareAdapterIpRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, HardwareAdapter::class);
    }

//    /**
//     * @return HardwareAdapterIp[] Returns an array of HardwareAdapterIp objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HardwareAdapterIp
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
