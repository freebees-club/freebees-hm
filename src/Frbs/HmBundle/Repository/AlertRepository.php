<?php

namespace App\Frbs\HmBundle\Repository;

use App\Frbs\HmBundle\Entity\Alert;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Alert|null find($id, $lockMode = null, $lockVersion = null)
 * @method Alert|null findOneBy(array $criteria, array $orderBy = null)
 * @method Alert[]    findAll()
 * @method Alert[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AlertRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Alert::class);
    }

    public function findDeviceWithAlerts()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->select('a', 'd')
            ->from('App\Frbs\HmBundle\Entity\Alert', 'a')
            ->leftJoin('a.device', 'd')
            ->groupBy('a.device')
        ;

        $result = $qb->getQuery()->getArrayResult();

        return $result;
    }

    public function findByAlertType($type)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->select('a')
            ->from('App\Frbs\HmBundle\Entity\alert', 'a')
            ->where('a.type = :type')
            ->setParameter('type', $type)
        ;

        $result = $qb->getQuery()->getArrayResult();

        return $result;
    }    

}
