<?php

/*
 * Freebees.ru
 */

/**
 * Description of Controller
 *
 * @author SAM
 */

namespace App\Frbs\HmBundle\Controller;

use App\Frbs\HmBundle\Entity\Alert;
use App\Frbs\HmBundle\Entity\Device;
use App\Frbs\HmBundle\Entity\DeviceGroup;
use App\Frbs\HmBundle\Entity\Log;
use App\Frbs\HmBundle\Entity\Settings;
use App\Frbs\HmBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class HmController extends FOSRestController
{
    /**
     * @Rest\Get("/api/v1/menu")
     */
    public function getMenuAction()
    {
        $group = $this->getDoctrine()
            ->getRepository(DeviceGroup::class)
            ->findAll();

        $reports = [
            ['name' => 'Статистика', 'url' => '/report/statistics', 'icon' => 'icon-list'],
            ['name' => 'События', 'url' => '/report/alerts', 'icon' => 'icon-list'],
            ['name' => 'ОС', 'url' => '/report/os', 'icon' => 'icon-list'],
            ['name' => 'Процессор', 'url' => '/report/cpu', 'icon' => 'icon-list'],
            ['name' => 'Видеокарта', 'url' => '/report/gpu', 'icon' => 'icon-list'],
            ['name' => 'Мат. плата', 'url' => '/report/mb', 'icon' => 'icon-list'],
            ['name' => 'Жесткий диск', 'url' => '/report/hdd', 'icon' => 'icon-list'],
            ['name' => 'Монитор', 'url' => '/report/display', 'icon' => 'icon-list'],
            ['name' => 'Память', 'url' => '/report/ram', 'icon' => 'icon-list'],
        ];

        $settings = [
            ['name' => 'Общие', 'url' => '/settings/common', 'icon' => 'icon-settings'],
            ['name' => 'Группы', 'url' => '/settings/groups', 'icon' => 'icon-settings'],
            ['name' => 'Пользователи', 'url' => '/settings/users', 'icon' => 'icon-settings'],
        ];

        $data = [['name' => 'Компьютеры', 'url' => '/computer', 'icon' => 'icon-screen-desktop', 'children' => $group]];
        array_push($data, ['name' => 'Отчеты', 'url' => '/report', 'icon' => 'icon-list', 'children' => $reports]);
        array_push($data, ['name' => 'Настройки', 'url' => '/settings', 'icon' => 'icon-settings', 'children' => $settings]);
        array_push($data, ['name' => 'Журнал', 'url' => '/log', 'icon' => 'icon-grid']);
        array_push($data, ['name' => 'Помощь', 'url' => '/help', 'icon' => 'icon-question']);

        return $this->view($data);

    }

    /**
     * Get app users
     *
     * @Rest\Get("/api/v1/user")
     */
    public function getAppUserListAction()
    {
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findAll();

        return $this->view($user);
    }

    /**
     * Add app user
     *
     * @Rest\Post("/api/v1/user")
     */
    public function addAppUserAction(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $data = json_decode($request->getContent(), true);

        $user = new User();

        $user->setFirstName($data['first_name']);
        $user->setSecondName($data['second_name']);
        $user->setThirdName($data['third_name']);
        $user->setUsername($data['username']);
        $user->setEmail($data['email']);
        $user->setIsActive($data['is_active']);
        $user->setRole($data['role']);
        $encoded = $encoder->encodePassword($user, $data['password']);
        $user->setPassword($encoded);
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return $this->view($user);
    }

    /**
     * Update app user
     *
     * @Rest\Put("/api/v1/user/{id}")
     * @ParamConverter("user", class="App\Frbs\HmBundle\Entity\User")
     */
    public function updateAppUserAction(Request $request, User $user, UserPasswordEncoderInterface $encoder)
    {
        $data = json_decode($request->getContent(), true);

        $user->setFirstName($data['first_name']);
        $user->setSecondName($data['second_name']);
        $user->setThirdName($data['third_name']);
        $user->setUsername($data['username']);
        $user->setEmail($data['email']);
        $user->setIsActive($data['is_active']);
        $user->setRole($data['role']);
        $encoded = $encoder->encodePassword($user, $data['password']);
        $user->setPassword($encoded);
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return $this->view($user);
    }

    /**
     * Delete app user
     *
     * @Rest\Delete("/api/v1/user/{id}")
     * @ParamConverter("user", class="App\Frbs\HmBundle\Entity\User")
     */
    public function deleteAppUserAction(User $user)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        return $this->view($user);
    }

    /**
     * Get search results
     *
     * @Rest\Get("/api/v1/search/{term}")
     */
    public function getSearchResultsAction(Request $request)
    {
        $term = $request->get('term');
        $device = $this->getDoctrine()
            ->getRepository(Device::class)
            ->findAllByTerm($term);

        return $this->view($device);
    }

    /**
     * Get groups
     *
     * @Rest\Get("/api/v1/group")
     */
    public function getGroupListAction()
    {
        $group = $this->getDoctrine()
            ->getRepository(DeviceGroup::class)
            ->findAll();

        return $this->view($group);
    }

    /**
     * Set group
     *
     * @Rest\Post("/api/v1/group")
     */
    public function setGroupAction(Request $request)
    {
        $group = new DeviceGroup;

        $data = json_decode($request->getContent(), true);
        $group->setName($data['name']);
        $group->setUrl('/device-group/');
        $group->setIcon('icon-screen-desktop');

        $em = $this->getDoctrine()->getManager();
        $em->persist($group);
        $em->flush();

        $group->setUrl('/device-group/' . $group->getId());
        $em->persist($group);
        $em->flush();

        return $this->view($group);
    }

    /**
     * Update group
     *
     * @Rest\Put("/api/v1/group/{id}")
     * @ParamConverter("group", class="App\Frbs\HmBundle\Entity\DeviceGroup")
     */
    public function updateGroupAction(Request $request, DeviceGroup $group)
    {
        $data = json_decode($request->getContent(), true);
        $group->setName($data['name']);
        $group->setUrl('/device-group/' . $data['id']);
        $group->setIcon('icon-screen-desktop');

        $em = $this->getDoctrine()->getManager();
        $em->persist($group);
        $em->flush();

        return $this->view($group);
    }

    /**
     * Update groups
     *
     * @Rest\Put("/api/v1/group")
     */
    public function updateGroupsAction(Request $request)
    {
        $groups = json_decode($request->getContent(), true);

        foreach ($groups as $item) {
            $group = $this->getDoctrine()
                ->getRepository(DeviceGroup::class)
                ->findOneBy(['id' => $item['id']]);
            $group->setName($item['name']);
            $em = $this->getDoctrine()->getManager();
            $em->persist($group);
        }

        $em->flush();

        return $this->view($groups);
    }

    /**
     * Delete group
     *
     * @Rest\Delete("/api/v1/group/{id}")
     * @ParamConverter("group", class="App\Frbs\HmBundle\Entity\DeviceGroup")
     */
    public function deleteGroupAction(DeviceGroup $group)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($group);
        $em->flush();

        return $this->view($group);
    }

    /**
     * Delete device alert
     *
     * @Rest\Delete("/api/v1/device/alert/{id}")
     * @ParamConverter("alert", class="App\Frbs\HmBundle\Entity\Alert")
     */
    public function deleteDeviceAlertAction(Alert $alert)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($alert);
        $em->flush();

        return $this->view($alert);
    }

    /**
     * Get devices of group
     *
     * @Rest\Get("/api/v1/device/group/{id}")
     */
    public function getGroupDetailAction(Request $request)
    {
        $id = $request->get('id');

        $device = $this->getDoctrine()
            ->getRepository(Device::class)
            ->findBy(['group' => $id], ['name' => 'ASC']);

        if (!$device) {
            throw $this->createNotFoundException(
                'No records found'
            );
        }

        return $this->view($device);
    }

    /**
     * Move devices to group
     *
     * @Rest\Put("/api/v1/device/move-to-group/{id}")
     * @ParamConverter("device", class="App\Frbs\HmBundle\Entity\DeviceGroup")
     */

    public function moveDevicesAction(Request $request, DeviceGroup $group)
    {

        $devices = json_decode($request->getContent(), true);

        foreach ($devices as $item) {
            $device = $this->getDoctrine()
                ->getRepository(Device::class)
                ->findOneBy(['id' => $item['id']]);
            $device->setGroup($group);

            $em = $this->getDoctrine()->getManager();
            $em->persist($device);
        }

        $em->flush();

        return $this->view($devices);

    }

    /**
     * Delete single device
     *
     * @Rest\Delete("/api/v1/device/{id}")
     * @ParamConverter("device", class="App\Frbs\HmBundle\Entity\Device")
     */
    public function deleteDeviceAction(Device $device)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($device);
        $em->flush();

        return $this->view($device);
    }

    /**
     * Delete multiple devices
     *
     * @Rest\Put("/api/v1/devices-delete")
     */
    public function deleteDevicesAction(Request $request)
    {
        $devices = json_decode($request->getContent(), true);

        $em = $this->getDoctrine()->getManager();

        foreach ($devices as $item) {
            $device = $this->getDoctrine()
                ->getRepository(Device::class)
                ->findOneBy(['id' => $item['id']]);
            $em->remove($device);
        }

        $em->flush();

        return $this->view($devices);
    }

    /**
     * Get device detailed information
     *
     * @Rest\Get("/api/v1/device-detail/{id}")
     */
    public function getDeviceDetailsAction(Request $request)
    {
        $id = $request->get('id');

        $device = $this->getDoctrine()
            ->getRepository(Device::class)
            ->findBy(['id' => $id]);

        if (!$device) {
            throw $this->createNotFoundException(
                'No records found'
            );
        }

        return $this->view($device);
    }

    /**
     * Get device alert list
     *
     * @Rest\Get("/api/v1/device/alert/{id}")
     * @ParamConverter("device", class="App\Frbs\HmBundle\Entity\Device")
     */
    public function getDeviceAlertListAction(Request $request, Device $device)
    {
        $alert = $device->getAlertList();

        if (!$alert) {
            throw $this->createNotFoundException(
                'No records found'
            );
        }

        return $this->view($alert);
    }

    /**
     * Get device software list
     *
     * @Rest\Get("/api/v1/device/software/{id}")
     * @ParamConverter("device", class="App\Frbs\HmBundle\Entity\Device")
     */
    public function getDeviceSoftwareListAction(Request $request, Device $device)
    {
        $software = $device->getSoftwareList();

        if (!$software) {
            throw $this->createNotFoundException(
                'No records found'
            );
        }

        return $this->view($software);
    }

    /**
     * Get device display list
     *
     * @Rest\Get("/api/v1/device/display/{id}")
     * @ParamConverter("device", class="App\Frbs\HmBundle\Entity\Device")
     */
    public function getDeviceDisplayListAction(Request $request, Device $device)
    {
        $display = $device->getDisplayList();

        if (!$display) {
            throw $this->createNotFoundException(
                'No records found'
            );
        }

        return $this->view($display);
    }

    /**
     * Get device printer list
     *
     * @Rest\Get("/api/v1/device/printer/{id}")
     * @ParamConverter("device", class="App\Frbs\HmBundle\Entity\Device")
     */
    public function getDevicePrinterListAction(Request $request, Device $device)
    {
        $printer = $device->getPrinterList();

        if (!$printer) {
            throw $this->createNotFoundException(
                'No records found'
            );
        }

        return $this->view($printer);
    }

    /**
     * Get device volume list
     *
     * @Rest\Get("/api/v1/device/volume/{id}")
     * @ParamConverter("device", class="App\Frbs\HmBundle\Entity\Device")
     */
    public function getDeviceVolumeListAction(Request $request, Device $device)
    {
        $volume = $device->getVolumeList();

        if (!$volume) {
            throw $this->createNotFoundException(
                'No records found'
            );
        }

        return $this->view($volume);
    }

    /**
     * Get device local user list
     *
     * @Rest\Get("/api/v1/device/local-user/{id}")
     * @ParamConverter("device", class="App\Frbs\HmBundle\Entity\Device")
     */
    public function getDeviceLocalUserListAction(Request $request, Device $device)
    {
        $local_user = $device->getLocalUserList();

        if (!$local_user) {
            throw $this->createNotFoundException(
                'No records found'
            );
        }

        return $this->view($local_user);
    }

    /**
     * Get device adapter list
     *
     * @Rest\Get("/api/v1/device/adapter/{id}")
     * @ParamConverter("device", class="App\Frbs\HmBundle\Entity\Device")
     */
    public function getDeviceAdapterListAction(Request $request, Device $device)
    {
        $adapter = $device->getAdapterList();

        if (!$adapter) {
            throw $this->createNotFoundException(
                'No records found'
            );
        }

        return $this->view($adapter);
    }

    /**
     * Get device cpu list
     *
     * @Rest\Get("/api/v1/device/cpu/{id}")
     * @ParamConverter("device", class="App\Frbs\HmBundle\Entity\Device")
     */
    public function getDeviceCpuListAction(Request $request, Device $device)
    {
        $cpu = $device->getCpuList();

        if (!$cpu) {
            throw $this->createNotFoundException(
                'No records found'
            );
        }

        return $this->view($cpu);
    }

    /**
     * Get device gpu list
     *
     * @Rest\Get("/api/v1/device/gpu/{id}")
     * @ParamConverter("device", class="App\Frbs\HmBundle\Entity\Device")
     */
    public function getDeviceGpuListAction(Request $request, Device $device)
    {
        $gpu = $device->getGpuList();

        if (!$gpu) {
            throw $this->createNotFoundException(
                'No records found'
            );
        }

        return $this->view($gpu);
    }

    /**
     * Get device hdd list
     *
     * @Rest\Get("/api/v1/device/hdd/{id}")
     * @ParamConverter("device", class="App\Frbs\HmBundle\Entity\Device")
     */
    public function getDeviceHddListAction(Request $request, Device $device)
    {
        $hdd = $device->getHddList();

        if (!$hdd) {
            throw $this->createNotFoundException(
                'No records found'
            );
        }

        return $this->view($hdd);
    }

    /**
     * Get device mb list
     *
     * @Rest\Get("/api/v1/device/mb/{id}")
     * @ParamConverter("device", class="App\Frbs\HmBundle\Entity\Device")
     */
    public function getDeviceMbListAction(Request $request, Device $device)
    {
        $mb = $device->getMbList();

        if (!$mb) {
            throw $this->createNotFoundException(
                'No records found'
            );
        }

        return $this->view($mb);
    }

    /**
     * Get device ram list
     *
     * @Rest\Get("/api/v1/device/ram/{id}")
     * @ParamConverter("device", class="App\Frbs\HmBundle\Entity\Device")
     */
    public function getDeviceRamListAction(Request $request, Device $device)
    {
        $ram = $device->getRamList();

        if (!$ram) {
            throw $this->createNotFoundException(
                'No records found'
            );
        }

        return $this->view($ram);
    }

    /**
     * Get alert list
     *
     * @Rest\Get("/api/v1/alert")
     */
    public function getAlertListAction(Request $request)
    {
        $alert = $this->getDoctrine()
            ->getRepository(Alert::class)
            ->findAll();

        if (!$alert) {
            throw $this->createNotFoundException(
                'No records found'
            );
        }

        return $this->view($alert);
    }

    /**
     * Get dashboard
     *
     * @Rest\Get("/api/v1/dashboard")
     */
    public function getDashboardAction(Request $request)
    {

        $device = $this->getDoctrine()
            ->getRepository(Device::class)
            ->findAll();

        $group = $this->getDoctrine()
            ->getRepository(DeviceGroup::class)
            ->findAll();

        $data = [
            'device_count' => count($device),
            'group_count' => count($group),
            'system_version' => '0.0.3',
        ];

        return $this->view($data);

    }

    /**
     * Get Settings
     *
     * @Rest\Get("/api/v1/settings")
     */
    public function getSettingsAction(Request $request)
    {
        $settings = $this->getDoctrine()
            ->getRepository(Settings::class)
            ->findOneBy(['id' => 1]);

        return $this->view($settings);
    }

    /**
     * Update settings
     *
     * @Rest\Put("/api/v1/settings")
     */
    public function setSettingsAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $settings = $this->getDoctrine()
            ->getRepository(Settings::class)
            ->findOneBy(['id' => 1]);

        if (!$settings) {
            $settings = new Settings();
        }

        $settings->setValue($data['value']);
        $settings->setCreatedAt(new \DateTime);

        $em = $this->getDoctrine()->getManager();
        $em->persist($settings);
        $em->flush();

        return $this->view($settings);
    }

    /**
     * Report statistics
     *
     * @Rest\Get("/api/v1/report/statistics")
     */
    public function getReportStatisticsAction(Request $request)
    {
        $result = [];

        $device = $this->getDoctrine()
            ->getRepository(Device::class)
            ->findAll();

        $alert = $this->getDoctrine()
            ->getRepository(Alert::class)
            ->findAll();

        $log = $this->getDoctrine()
            ->getRepository(Log::class)
            ->findAll();

        $deviceWithAlerts = $this->getDoctrine()
            ->getRepository(Alert::class)
            ->findDeviceWithAlerts();

        $alertvBat = $this->getDoctrine()
            ->getRepository(Alert::class)
            ->findByAlertType(1);

        $alertCpuTemp = $this->getDoctrine()
            ->getRepository(Alert::class)
            ->findByAlertType(2);

        $alertHddTemp = $this->getDoctrine()
            ->getRepository(Alert::class)
            ->findByAlertType(3);

        $alertVolumeFree = $this->getDoctrine()
            ->getRepository(Alert::class)
            ->findByAlertType(4);

        $alertGpuTemp = $this->getDoctrine()
            ->getRepository(Alert::class)
            ->findByAlertType(5);

        $result['device_count'] = count($device);
        $result['alert_count'] = count($alert);
        $result['log_count'] = count($log);
        $result['device_with_alerts'] = count($deviceWithAlerts);
        $result['alert_vbat'] = count($alertvBat);
        $result['alert_cpu_temp'] = count($alertCpuTemp);
        $result['alert_hdd_temp'] = count($alertHddTemp);
        $result['alert_volume_free'] = count($alertVolumeFree);
        $result['alert_gpu_temp'] = count($alertGpuTemp);

        return $this->view($result);
    }

    /**
     * Report alerts
     *
     * @Rest\Get("/api/v1/report/alerts")
     */
    public function getReportAlertsAction(Request $request)
    {
        $result = $this->getDoctrine()
            ->getRepository(Alert::class)
            ->findBy([], ['created_at' => 'DESC']);

        if (!$result) {
            throw $this->createNotFoundException(
                'No records found'
            );
        }

        return $this->view($result);
    }

    /**
     * Report os
     *
     * @Rest\Get("/api/v1/report/os")
     */
    public function getReportOsAction(Request $request)
    {
        $result = $this->getDoctrine()
            ->getRepository(Device::class)
            ->findBy([], ['name' => 'ASC']);

        if (!$result) {
            throw $this->createNotFoundException(
                'No records found'
            );
        }

        return $this->view($result);
    }

    /**
     * Report ram
     *
     * @Rest\Get("/api/v1/report/ram")
     */
    public function getReportRamAction(Request $request)
    {
        $result = $this->getDoctrine()
            ->getRepository(Device::class)
            ->findAllRam();

        if (!$result) {
            throw $this->createNotFoundException(
                'No records found'
            );
        }

        return $this->view($result);
    }

    /**
     * Report cpu
     *
     * @Rest\Get("/api/v1/report/cpu")
     */
    public function getReportCpuAction(Request $request)
    {
        $result = $this->getDoctrine()
            ->getRepository(Device::class)
            ->findAllCpu();

        if (!$result) {
            throw $this->createNotFoundException(
                'No records found'
            );
        }

        return $this->view($result);
    }

    /**
     * Report gpu
     *
     * @Rest\Get("/api/v1/report/gpu")
     */
    public function getReportGpuAction(Request $request)
    {
        $result = $this->getDoctrine()
            ->getRepository(Device::class)
            ->findAllGpu();

        if (!$result) {
            throw $this->createNotFoundException(
                'No records found'
            );
        }

        return $this->view($result);
    }

    /**
     * Report hdd
     *
     * @Rest\Get("/api/v1/report/hdd")
     */
    public function getReportHddAction(Request $request)
    {
        $result = $this->getDoctrine()
            ->getRepository(Device::class)
            ->findAllHdd();

        if (!$result) {
            throw $this->createNotFoundException(
                'No records found'
            );
        }

        return $this->view($result);
    }

    /**
     * Report display
     *
     * @Rest\Get("/api/v1/report/display")
     */
    public function getReportDisplayAction(Request $request)
    {
        $result = $this->getDoctrine()
            ->getRepository(Device::class)
            ->findAllDisplay();

        if (!$result) {
            throw $this->createNotFoundException(
                'No records found'
            );
        }

        return $this->view($result);
    }

    /**
     * Report mb
     *
     * @Rest\Get("/api/v1/report/mb")
     */
    public function getReportMbAction(Request $request)
    {
        $result = $this->getDoctrine()
            ->getRepository(Device::class)
            ->findAllMb();

        if (!$result) {
            throw $this->createNotFoundException(
                'No records found'
            );
        }

        return $this->view($result);
    }

    /**
     * Get log
     *
     * @Rest\Get("/api/v1/log")
     */
    public function getLogAction(Request $request)
    {
        $result = $this->getDoctrine()
            ->getRepository(Log::class)
            ->findBy([], ['created_at' => 'DESC']);

        if (!$result) {
            throw $this->createNotFoundException(
                'No records found'
            );
        }

        return $this->view($result);
    }

    /**
     * Set log
     *
     * @Rest\Put("/api/v1/log")
     */
    public function setLogAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        
        $log = new Log();
        $log->setValue($data['value']);

        $em = $this->getDoctrine()->getManager();
        $em->persist($log);
        $em->flush();

        return $this->view($log);
    }

    /**
     * Flush log
     *
     * @Rest\Delete("/api/v1/log")
     */
    public function flushLogAction(Request $request)
    {
        $result = $this->getDoctrine()
            ->getRepository(Log::class)
            ->findAll();

        $em = $this->getDoctrine()->getManager();

        foreach ($result as $item) {
            $log = $this->getDoctrine()
                ->getRepository(Log::class)
                ->findOneBy(['id' => $item->getId()]);
            $em->remove($log);
        }

        $em->flush();

        return $this->view($result);
    }
}
