#!/bin/bash

if [ ! -f /var/lib/mysql/ibdata1 ]; then

    mysql_install_db

    /usr/bin/mysqld_safe &
    sleep 10s

    echo "GRANT ALL ON *.* TO root@'localhost' WITH GRANT OPTION; FLUSH PRIVILEGES" | mysql

    mysql -u root -e "use mysql;update user set authentication_string=password(''),plugin='mysql_native_password' where user='root';FLUSH PRIVILEGES;" && \
    php /var/www/freebees-hm/bin/console doctrine:database:create
    php /var/www/freebees-hm/bin/console doctrine:schema:create
    php /var/www/freebees-hm/bin/console doctrine:fixtures:load --append

    killall mysqld
    sleep 10s

fi

/usr/bin/mysqld_safe

# Start apache
/usr/sbin/apache2 -D FOREGROUND
# Start node service
pm2-runtime /usr/freebees-hm-service/service.js